;----------------------------------------------
; OwlGalunggung Windows NSIS Macros Header
; [Functions.nsh]
; 
;  Copyright (C) 2017-2018 Hyang Language Foundation
;  Contributors:
;  Elvira Larasati <laras@owlgalunggung.org>
;  Anna Nita <annita@owlgalunggung.org>
;  Aldi Dinata <dinataldi@owlgalunggung.org>
;----------------------------------------------

!define OWLGAL_FILE_CLASSES "owlgal_must_be_first,owlgaldifffile,owlgalaspfile,owlgalshfile,owlgalprojectfile,owlgallang2file,owlgalcfile,owlgalhfile,owlgalcppfile,owlgalhppfile,owlgalcssfile,owlgaldfile,owlgalpofile,owlgaljsfile,owlgaljspfile,owlgalnsifile,owlgalnshfile,owlgalhyssfile,owlgaltxtfile,owlgalhyangfile,owlgalsmartyfile,owlgalvbsfile,owlgalxhtmlfile,owlgalxmlfile,owlgalxslfile,owlgalplfile,owlgalrbfile,owlgalpyfile"
Function FileAssociations
	!insertmacro MUI_HEADER_TEXT "$(FA_TITLE)" "$(FA_HEADER)"
	nsDialogs::Create 1018
	Pop $FA_Dialog
	${If} $FA_Dialog == "error"
		Abort
	${EndIf}

	${NSD_CreateCheckBox} 5% 10u 40% 8u "ActiveServer Pages (.asp)"
	Pop $FA_Asp
	${SelectIfRegistered} $FA_Asp "asp"
	${NSD_CreateCheckBox} 5% 20u 40% 8u "Bash (.sh)"
	Pop $FA_Sh
	${SelectIfRegistered} $FA_Sh "sh"
	${NSD_CreateCheckBox} 5% 30u 40% 8u "Owl Galunggung Project (.owlgal)"
	Pop $FA_OwlGalProject
	${NSD_Check} $FA_OwlGalProject
	${NSD_CreateCheckBox} 5% 40u 40% 8u "Owl Galunggung Language (.owlgallang2)"
	Pop $FA_OwlGalLang2
	${NSD_Check} $FA_OwlGalLang2
	${NSD_CreateCheckBox} 5% 50u 40% 8u "C (.c; .h)"
	Pop $FA_C
	${SelectIfRegistered} $FA_C "c"
	${SelectIfRegistered} $FA_C "h"
	${NSD_CreateCheckBox} 5% 60u 40% 8u "C++ (.cpp; .cxx; .cc; .hpp)"
	Pop $FA_Cpp
	${SelectIfRegistered} $FA_Cpp "cpp"
	${SelectIfRegistered} $FA_Cpp "cxx"
	${SelectIfRegistered} $FA_Cpp "cc"
	${SelectIfRegistered} $FA_Cpp "hpp"
	${NSD_CreateCheckBox} 5% 70u 40% 8u "CSS (.css)"
	Pop $FA_Css
	${SelectIfRegistered} $FA_Css "css"
	${NSD_CreateCheckBox} 5% 80u 40% 8u "D (.d)"
	Pop $FA_D
	${SelectIfRegistered} $FA_D "d"
	${NSD_CreateCheckBox} 5% 90u 40% 8u "Diff (.diff; .patch)"
	Pop $FA_Diff
	${SelectIfRegistered} $FA_Diff "diff"
	${SelectIfRegistered} $FA_Diff "patch"
	${NSD_CreateCheckBox} 5% 100u 40% 8u "Gettext PO (.po)"
	Pop $FA_Po
	${SelectIfRegistered} $FA_Po "po"
	${NSD_CreateCheckBox} 5% 110u 40% 8u "HTML (.htm; .html)"
	Pop $FA_Html
	${SelectIfRegisteredHTML}

	${NSD_CreateCheckBox} 55% 0u 40% 8u "JavaScript (.js)"
	Pop $FA_Js
	${SelectIfRegistered} $FA_Js "js"
	${NSD_CreateCheckBox} 55% 10u 40% 8u "JavaServer Pages (.jsp)"
	Pop $FA_Jsp
	${SelectIfRegistered} $FA_Jsp "jsp"
	${NSD_CreateCheckBox} 55% 30u 40% 8u "NSIS (.nsi; .nsh)"
	Pop $FA_Nsi
	${SelectIfRegistered} $FA_Nsi "nsi"
	${SelectIfRegistered} $FA_Nsi "nsh"
	${NSD_CreateCheckBox} 55% 50u 40% 8u "HYSS (.hyss; .hyss-xx)"
	Pop $FA_HySS
	${SelectIfRegistered} $FA_HySS "hyss"
	${SelectIfRegistered} $FA_HySS "hyss-xx"
	${NSD_CreateCheckBox} 55% 60u 40% 8u "Plain Text (.txt)"
	Pop $FA_Txt
	${SelectIfRegistered} $FA_Txt "txt"
	${NSD_CreateCheckBox} 55% 70u 40% 8u "Hyang (.hyang)"
	Pop $FA_Hyang
	${SelectIfRegistered} $FA_Hyang "hyang"
	${NSD_CreateCheckBox} 55% 90u 40% 8u "Smarty (.tpl)"
	Pop $FA_Smarty
	${SelectIfRegistered} $FA_Smarty "tpl"
	${NSD_CreateCheckBox} 55% 100u 40% 8u "VisualBasic Script (.vbs; .vb)"
	Pop $FA_Vbs
	${SelectIfRegistered} $FA_Vbs "vbs"
	${SelectIfRegistered} $FA_Vbs "vb"
	${NSD_CreateCheckBox} 55% 110u 40% 8u "XHTML (.xhtml)"
	Pop $FA_Xhtml
	${SelectIfRegistered} $FA_Xhtml "xhtml"
	${NSD_CreateCheckBox} 55% 120u 40% 8u "XML (.xml; .xsl)"
	Pop $FA_Xml
	${SelectIfRegistered} $FA_Xml "xml"
	${SelectIfRegistered} $FA_Xml "xsl"
	${NSD_CreateCheckBox} 55% 40u 40% 8u "Perl (.pl)"
	Pop $FA_Pl
	${SelectIfRegistered} $FA_Pl "pl"
	${NSD_CreateCheckBox} 55% 80u 40% 8u "Ruby (.rb)"
	Pop $FA_Rb
	${SelectIfRegistered} $FA_Rb "rb"
	${NSD_CreateCheckBox} 55% 70u 40% 8u "Python (.py)"
	Pop $FA_Py
	${SelectIfRegistered} $FA_Py "py"

	${NSD_CreateCheckBox} 40% 130u 30% 8u "$(FA_SELECT)"
	Pop $FA_SelectAll
	${NSD_OnClick} $FA_SelectAll FileAssociations_SelectAll

	nsDialogs::Show
FunctionEnd

; Note: Make sure to add or remove handlers from the OWLGAL_FILE_CLASSES define when changing
Function SetFileAssociations
	;                     HWND			Extension	Mime Type		Handler	Content Type	ICON Id
	${RegisterFileType} $FA_Asp  	"asp" 	"text/x-asp" 			"owlgalaspfile" "$(CT_ASP)" 2
	${RegisterFileType} $FA_Sh  	"sh" 	"text/x-shellscript" 		"owlgalshfile" "$(CT_SH)" 22
	${RegisterFileType} $FA_OwlGalProject 	"owlgalproject" 	"application/x-owlgalunggung-project" 	"owlgalprojectfile" "$(CT_OWLGALPROJECT)" 4
	${RegisterFileType} $FA_OwlGalLang2 	"owlgallang2" 	"application/x-owlgalunggung-language2" 		"owlgallang2file" "$(CT_OWLGALLANG2)" 3
	${RegisterFileType} $FA_C  		"c" 	"text/x-csrc" 			"owlgalcfile" "$(CT_C)" 5
	${RegisterFileType} $FA_C  		"h" 	"text/x-chdr" 			"owlgalhfile" "$(CT_H)" 10
	${RegisterFileType} $FA_Cpp 	"cpp" 	"text/x-c++src" 		"owlgalcppfile" "$(CT_CPP)" 6
	${RegisterFileType} $FA_Cpp 	"cxx" 	"text/x-c++src" 		"owlgalcppfile" "$(CT_CPP)" 6
	${RegisterFileType} $FA_Cpp 	"cc" 		"text/x-c++src" 		"owlgalcppfile" "$(CT_CPP)" 6
	${RegisterFileType} $FA_Cpp 	"hpp" 	"text/x-c++hdr" 		"owlgalhppfile" "$(CT_HPP)" 11
	${RegisterFileType} $FA_Css 	"css" 	"text/css" 			"owlgalcssfile" "$(CT_CSS)" 7
	${RegisterFileType} $FA_D 		"d" 		"text/x-dsrc" 			"owlgaldfile" "$(CT_D)" 8
	${RegisterFileType} $FA_Diff 	"diff" 	"text/x-diff" 			"owlgaldifffile" "$(CT_DIFF)" 100
	${RegisterFileType} $FA_Diff 	"patch" 	"text/x-patch" 			"owlgaldifffile" "$(CT_DIFF)" 100
	${RegisterFileType} $FA_Po  	"po" 		"text/x-gettext-translation" 	"owlgalpofile" "$(CT_PO)" 9
	${RegisterFileType} $FA_Jsp 	"jsp" 	"application/x-jsp" 		"owlgaljspfile" "$(CT_JSP)" 15
	${RegisterFileType} $FA_Nsi 	"nsi" 	"text/x-nsi" 			"owlgalnsifile" "$(CT_NSI)" 17
	${RegisterFileType} $FA_Nsi 	"nsh" 	"text/x-nsh" 			"owlgalnshfile" "$(CT_NSH)" 16
	${RegisterFileType} $FA_HySS 	"hyss" 	"application/x-hyss" 		"owlgalhyssfile" "$(CT_HYSS)" 19
	${RegisterFileType} $FA_HySS 	"hyss-xx" 	"application/x-hyss" 		"owlgalhyssfile" "$(CT_HYSS)" 19
	${RegisterFileType} $FA_Txt 	"txt" 	"text/plain" 			"owlgaltxtfile" "$(CT_TXT)" 24
	${RegisterFileType} $FA_Hyang  	"hyang" 		"text/x-hyang" 		"owlgalhyangfile" "$(CT_HYANG)" 20
	${RegisterFileType} $FA_Smarty 	"tpl" 	"application/x-smarty" 		"owlgalsmartyfile" "$(CT_SMARTY)" 23
	${RegisterFileType} $FA_Vbs 	"vb" 	"application/x-vbscript" 		"owlgalvbsfile" "$(CT_VBS)" 25
	${RegisterFileType} $FA_Xhtml 	"xhtml" 	"application/xhtml+xml" 		"owlgalxhtmlfile" "$(CT_XHTML)" 26
	${RegisterFileType} $FA_Xml 	"xml" 	"text/xml" 			"owlgalxmlfile" "$(CT_XML)" 27
	${RegisterFileType} $FA_Xml 	"xsl" 	"application/xslt+xml" 		"owlgalxslfile" "$(CT_XSL)" 28
	${RegisterFileType} $FA_Xml 	"xslt" 	"application/xslt+xml" 		"owlgalxslfile" "$(CT_XSL)" 28
	${RegisterFileType} $FA_Pl  	"pl" 		"application/x-perl" "owlgalplfile" "$(CT_PL)" 18
	${RegisterFileType} $FA_Rb  	"rb" 		"text/x-ruby" 			"owlgalrbfile" "$(CT_RB)" 21
	${RegisterFileType} $FA_Py  	"py" 		"text/x-python" 		"owlgalpyfile" "$(CT_PY)" 20
    
	; HTML is a special case
	${RegisterHTMLType} $FA_Html
	${RegisterFileType} $FA_Html	"htm" 	"text/html" 		"0"	"0"	"0"
	${RegisterFileType} $FA_Html	"html" 	"text/html" 		"0"	"0"	"0"

	; Extra special cases
	${RegisterFileType} $FA_Js  	"js" 		"application/javascript" 		"owlgaljsfile" "$(CT_JS)" 14
	${RegisterFileType} $FA_Vbs 	"vbs" 		"application/x-vbscript" 		"owlgalvbsfile" "$(CT_VBS)" 25
FunctionEnd

Function FileAssociations_SelectAll
	${NSD_GetState} $FA_SelectAll $R0
	${If} $R0 == ${BST_CHECKED}
		${NSD_SetText} $FA_SelectAll "$(FA_UNSELECT)"
		${NSD_Check} $FA_Asp
		${NSD_Check} $FA_Sh
		${NSD_Check} $FA_OwlGalProject
		${NSD_Check} $FA_OwlGalLang2
		${NSD_Check} $FA_C
		${NSD_Check} $FA_Cpp
		${NSD_Check} $FA_Css
		${NSD_Check} $FA_D
		${NSD_Check} $FA_Diff
		${NSD_Check} $FA_Po
		${NSD_Check} $FA_Html
		${NSD_Check} $FA_Js
		${NSD_Check} $FA_Jsp
		${NSD_Check} $FA_Nsi
		${NSD_Check} $FA_HySS
		${NSD_Check} $FA_Txt
		${NSD_Check} $FA_Hyang
		${NSD_Check} $FA_Smarty
		${NSD_Check} $FA_Vbs
		${NSD_Check} $FA_Xhtml
		${NSD_Check} $FA_Xml
		${NSD_Check} $FA_Pl
		${NSD_Check} $FA_Rb
		${NSD_Check} $FA_Py

	${Else}
		${NSD_SetText} $FA_SelectAll "$(FA_SELECT)"
		${NSD_Uncheck} $FA_Asp
		${NSD_Uncheck} $FA_Sh
		${NSD_Uncheck} $FA_OwlGalProject
		${NSD_Uncheck} $FA_OwlGalLang2
		${NSD_Uncheck} $FA_C
		${NSD_Uncheck} $FA_Cpp
		${NSD_Uncheck} $FA_Css
		${NSD_Uncheck} $FA_D
		${NSD_Uncheck} $FA_Diff
		${NSD_Uncheck} $FA_Po
		${NSD_Uncheck} $FA_Html
		${NSD_Uncheck} $FA_Js
		${NSD_Uncheck} $FA_Jsp
		${NSD_Uncheck} $FA_Nsi
		${NSD_Uncheck} $FA_HySS
		${NSD_Uncheck} $FA_Txt
		${NSD_Uncheck} $FA_Hyang
		${NSD_Uncheck} $FA_Smarty
		${NSD_Uncheck} $FA_Vbs
		${NSD_Uncheck} $FA_Xhtml
		${NSD_Uncheck} $FA_Xml
		${NSD_Uncheck} $FA_Pl
		${NSD_Uncheck} $FA_Rb
		${NSD_Uncheck} $FA_Py
	${EndIf}
FunctionEnd

Function DisableBackButton
	Push $0
	GetDlGItem $0 $HWNDPARENT 3
	EnableWindow $0 0
	Pop $0
FunctionEnd

Function GtkVersionCheck
	Push $R0
	Push $R1
	Push $R2
	; Get the current user's installed version of GTK+ from the registry
	${If} $HKEY == "Classic"
		ReadRegStr $R0 HKCU "${REG_USER_SET}" "GTK"
	${Else}
		ReadRegStr $R0 HKLM "${REG_USER_SET}" "GTK"
	${EndIf}

	; If we were unable to retrieve the current GTK+ version from the registry
	;  we can assume that it is not currently installed
	StrLen $R1 $R0
	${If} $R1 == 0
		StrCpy $GTK_STATUS ""
	${Else}
		; Check the major version
		${StrTok} $R1 $R0 "." "0" "1"
		${StrTok} $R2 ${GTK_MIN_VERSION} "." "0" "1"
		${If} $R1 == $R2
			; Check the minor version
			${StrTok} $R1 $R0 "." "1" "1"
			${StrTok} $R2 ${GTK_MIN_VERSION} "." "1" "1"
			${If} $R1 == $R2
				; Check the patch level
				${StrTok} $R1 $R0 "." "2" "1"
				${StrTok} $R2 ${GTK_MIN_VERSION} "." "2" "1"
				${If} $R1 >= $R2
					StrCpy $GTK_STATUS "installed"
				${Else}
					StrCpy $GTK_STATUS ""
				${EndIf}
			${ElseIf} $R1 > $R2
				StrCpy $GTK_STATUS "installed"
			${Else}
				StrCpy $GTK_STATUS ""
			${EndIf}
		${ElseIf} $R1 > $R2
			StrCpy $GTK_STATUS "installed"	
		${Else}
			StrCpy $GTK_STATUS ""
		${EndIf}
	${EndIf}
	Pop $R2
	Pop $R1
	Pop $R0
FunctionEnd

Function HyangVersionCheck
; Check if Hyang is installed as the Current User
	StrCpy $0 0
HyangClassLoop:
	EnumRegKey $1 HKCU "Software\Hyang\hyang" $0
	StrCmp $1 "" HyangAdmin 0 ; If not installed as CU check Admin
	IntOp $0 $0 + 1
	StrCmp $1 "2.7" HyangFinish HyangClassLoop

; Check if Hyang is installed as Local Administrator
HyangAdmin:
	StrCpy $0 0
HyangAdminLoop:
	EnumRegKey $1 HKLM "Software\Hyang\hyang" $0
	StrCmp $1 "" HyangFail 0 ; If not, Hyang is not installed
	IntOp $0 $0 + 1
	StrCmp $1 "2.7" HyangFinish HyangAdminLoop

HyangFail:
	StrCpy $HYANG_STATUS ""
	Return
HyangFinish:
	StrCpy $HYANG_STATUS "installed"
	Return
FunctionEnd
