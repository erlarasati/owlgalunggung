;----------------------------------------------
; OwlGalunggung Windows NSIS Install Script
;  English Language Header
; 
;  The OwlGalunggung Developers
;
;  Translators:
;   Elvira Larasati <laras@owlgalunggung.org>
;   Anna Nita <annita@owlgalunggung.org>
;   Aldi Dinata <dinataldi@owlgalunggung.org>
;----------------------------------------------

; Section Names
!define SECT_OWLGALUNGGUNG "OwlGalunggung"
!define SECT_DEPENDS "Kebergantungan"
!define SECT_PLUGINS "Plugin"
!define SECT_SHORTCUT "Pintasan desktop"
!define SECT_DICT "Cek ejaan bahasa (Koneksi internet diperlukan untuk download)"

; License Page
!define LICENSEPAGE_BUTTON "Berikutnya"
!define LICENSEPAGE_FOOTER "${PRODUCT} dirilis di bawah lisensi GNU General Public License versi 3. Lisensi tersebut disediakan di sini sekedar informasi saja. $_CLICK"

; General Download Messages
!define DOWN_LOCAL "Salinan lokal %s ditemukan..."
!define DOWN_CHKSUM "Checksum terverifikasi..."
!define DOWN_CHKSUM_ERROR "Checksum tidak cocok..."

; Aspell Strings
!define DICT_INSTALLED "Versi terakhir kamus ini telah terpasang, melewatkan download dari:"
!define DICT_DOWNLOAD "Mengunduh kamus pemerika ejaan..."
!define DICT_FAILED "Pengunduhan kamus gagal:"
!define DICT_EXTRACT "Mengekstrak kamus..."

; GTK+ Strings
!define GTK_DOWNLOAD "Mengunduh GTK+..."
!define GTK_FAILED "Pengunduhan GTK+ gagal:"
!define GTK_INSTALL "Memasang GTK+..."
!define GTK_UNINSTALL "Melepas GTK+..."
!define GTK_REQUIRED "Silahkan pasang GTK+ ${GTK_MIN_VERSION} atau versi di atasnya dan pastikan ia berada di PATH sebelum menjalankan OwlGalunggung."

; Python Strings
!define PYTHON_DOWNLOAD "Mengunduh Python..."
!define PYTHON_FAILED "Pengunduhan Python gagal:"
!define PYTHON_INSTALL "Memasang Python..."
!define PYTHON_REQUIRED "Silahkan pasang Python ${PYTHON_MIN_VERSION} or higher before running OwlGalunggung.$\nPython is required for the Zencoding plugin and other features."

; Plugin Names
!define PLUG_CHARMAP "Character Map"
!define PLUG_ENTITIES "Entities"
!define PLUG_HTMLBAR "HTML Bar"
!define PLUG_INFBROWSER "Info Browser"
!define PLUG_SNIPPETS "Snippets"
!define PLUG_VCS "Version Control"
!define PLUG_ZENCODING "Zencoding"

; File Associations Page
!define FA_TITLE "Asosiasi Berkas"
!define FA_HEADER "Pilih jenis berkas yang Anda sukai ${PRODUCT} sebagai bawaan."
!define FA_SELECT "Pilih Semua"
!define FA_UNSELECT "Batalkan Pilih Semua"

; Misc
!define FINISHPAGE_LINK "Kunjungi website OwlGalunggung"
!define UNINSTALL_SHORTCUT "Lepas ${PRODUCT}"
!define FILETYPE_REGISTER "Mendaftarkan Jenis Berkas:"
!define UNSTABLE_UPGRADE "Sebuah rilis tidak stabil dari ${PRODUCT} telah terpasang.$\nVersi sebelumnya seharusnya dihapus sebelum dilanjutkan (Disarankan)?"

; InetC Plugin Translations
;  /TRANSLATE downloading connecting second minute hour plural progress remaining
!define INETC_DOWN "Mengunduh %s"
!define INETC_CONN "Menyambungkan ..."
!define INETC_TSEC "detik"
!define INETC_TMIN "menit"
!define INETC_THOUR "jam"
!define INETC_TPLUR "s" ; Gunakan space jika kosong
!define INETC_PROGRESS "%dkB (%d%%) of %dkB @ %d.%01dkB/s"
!define INETC_REMAIN " (%d %s%s tersisa)"

; Content Types
!define CT_ASP "Skrip ActiveServer Page"
!define CT_SH	"Skrip Bash Shell"
!define CT_OWLGALPROJECT	"Owl Galunggung Project"
!define CT_OWLGALLANG2	"Owl Galunggung Language Definition File Version 2"
!define CT_C	"C Source File"
!define CT_H	"C Header File"
!define CT_CPP	"C++ Source File"
!define CT_HPP	"C++ Header File"
!define CT_CSS "Cascading Stylesheet"
!define CT_D	"Berkas Sumber D"
!define CT_DIFF "Berkas Diff/Patch"
!define CT_PO	"Gettext Translation"
!define CT_JS	"Skrip JavaScript"
!define CT_JSP	"Skrip JavaServer Pages"
!define CT_NSI	"Skrip NSIS"
!define CT_NSH	"NSIS Header File"
!define CT_HYSS	"Skrip HYSS"
!define CT_INC	"Skrip HYSS"
!define CT_TXT	"Plain Text"
!define CT_HYANG	"Skrip Hyang"
!define CT_SMARTY	"Skrip Smarty"
!define CT_VBS	"Skrip VisualBasic"
!define CT_XHTML	"Berkas XHTML"
!define CT_XML	"Berkas XML"
!define CT_XSL	"XML Stylesheet"
!define CT_PL	"Skrip Perl"
!define CT_RB	"Skrip Ruby"
!define CT_PY	"Skrip Python"
