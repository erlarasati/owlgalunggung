/* OwlGalunggung Code Editor
 * encodings_dialog.h
 *
 * Copyright (C) 2017-2018 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ENCODINGS_DIALOG_H_
#define	__ENCODINGS_DIALOG_H_

#include "owlgalunggung.h"


#define OWLGALUNGGUNG_TYPE_ENCODINGS_DIALOG            (owlgalunggung_encodings_dialog_get_type ())
#define OWLGALUNGGUNG_ENCODINGS_DIALOG(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), OWLGALUNGGUNG_TYPE_ENCODINGS_DIALOG, OwlGalunggungEncodingsDialog))
#define OWLGALUNGGUNG_ENCODINGS_DIALOG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass), OWLGALUNGGUNG_TYPE_ENCODINGS_DIALOG, OwlGalunggungEncodingsDialogClass))
#define OWLGALUNGGUNG_IS_ENCODINGS_DIALOG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), OWLGALUNGGUNG_TYPE_ENCODINGS_DIALOG))
#define OWLGALUNGGUNG_IS_ENCODINGS_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), OWLGALUNGGUNG_TYPE_ENCODINGS_DIALOG))
#define OWLGALUNGGUNG_ENCODINGS_DIALOG_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), OWLGALUNGGUNG_TYPE_ENCODINGS_DIALOG, OwlGalunggungEncodingsDialogClass))


typedef struct _OwlGalunggungEncodingsDialogPrivate OwlGalunggungEncodingsDialogPrivate;

typedef struct _OwlGalunggungEncodingsDialog OwlGalunggungEncodingsDialog;
  
struct _OwlGalunggungEncodingsDialog
{
  GtkDialog parent;
  
  OwlGalunggungEncodingsDialogPrivate *priv;
};

typedef struct _OwlGalunggungEncodingsDialogClass OwlGalunggungEncodingsDialogClass;

struct _OwlGalunggungEncodingsDialogClass
{
  GtkDialogClass parent_class;
};

GType owlgalunggung_Encodings_dialog_get_type (void);


void owlgalunggung_encodings_dialog_new (Towlgalwin *owlgalwin);


#endif	/* __ENCODINGS_DIALOG_H_ */
