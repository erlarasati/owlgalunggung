# English translations for owlgalunggung package.
# Copyright (C) 2017-2018 Hyang Language Foundation, Jakarta.
# This file is distributed under the same license as the owlgalunggung package.
# Automatically generated, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: owlgalunggung 2.0\n"
"Report-Msgid-Bugs-To: contributing@owlgalunggung.org\n"
"POT-Creation-Date: 2016-06-16 09:51+0200\n"
"PO-Revision-Date: 2018-09-13 08:46+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: src/plugin_snippets/snippets_gui.c:424
msgid "Click the right mouse button to add, edit or delete snippets."
msgstr "Click the right mouse button to add, edit or delete snippets."

#: src/plugin_snippets/snippets_gui.c:425
msgid "Use drag and drop to re-order snippets"
msgstr "Use drag and drop to re-order snippets"

#: src/plugin_snippets/snippets_gui.c:426
msgid "To exchange snippets with others use import or export"
msgstr "To exchange snippets with others use import or export"

#: src/plugin_snippets/snippets_gui.c:646
msgid "snippets"
msgstr "snippets"

#: src/plugin_snippets/snippets_gui.c:709
msgid "Snippet export filename"
msgstr "Snippet export filename"

#: src/plugin_snippets/snippets_gui.c:747
msgid "Snippet import filename"
msgstr "Snippet import filename"

#: src/plugin_snippets/snippets_gui.c:765
msgid "Set accelerator key"
msgstr "Set accelerator key"

#: src/plugin_snippets/snippets_gui.c:822
msgid "Snippets menu"
msgstr "Snippets menu"

#: src/plugin_snippets/snippets_gui.c:823
msgid "_New Snippet"
msgstr "_New Snippet"

#: src/plugin_snippets/snippets_gui.c:823
#: src/plugin_snippets/snippets_wizard.c:754
msgid "New snippet"
msgstr "New snippet"

#: src/plugin_snippets/snippets_gui.c:824
msgid "_Edit Snippet"
msgstr "_Edit Snippet"

#: src/plugin_snippets/snippets_gui.c:824
#: src/plugin_snippets/snippets_wizard.c:754
msgid "Edit snippet"
msgstr "Edit snippet"

#: src/plugin_snippets/snippets_gui.c:825
msgid "_Delete Snippet"
msgstr "_Delete Snippet"

#: src/plugin_snippets/snippets_gui.c:825
msgid "Delete snippet"
msgstr "Delete snippet"

#: src/plugin_snippets/snippets_gui.c:826
msgid "Delete _Branch"
msgstr "Delete _Branch"

#: src/plugin_snippets/snippets_gui.c:826
msgid "Delete branch"
msgstr "Delete branch"

#: src/plugin_snippets/snippets_gui.c:827
msgid "Set Snippet _Accelerator"
msgstr "Set Snippet _Accelerator"

#: src/plugin_snippets/snippets_gui.c:827
msgid "Set snippet accelerator"
msgstr "Set snippet accelerator"

#: src/plugin_snippets/snippets_gui.c:829
msgid "Ex_pand All"
msgstr "Ex_pand All"

#: src/plugin_snippets/snippets_gui.c:829
msgid "Expand all"
msgstr "Expand all"

#: src/plugin_snippets/snippets_gui.c:830
msgid "_Collapse All"
msgstr "_Collapse All"

#: src/plugin_snippets/snippets_gui.c:830
msgid "Collapse all"
msgstr "Collapse all"

#: src/plugin_snippets/snippets_gui.c:831
msgid "E_xport"
msgstr "E_xport"

#: src/plugin_snippets/snippets_gui.c:831
msgid "Export snippets"
msgstr "Export snippets"

#: src/plugin_snippets/snippets_gui.c:832
msgid "I_mport"
msgstr "I_mport"

#: src/plugin_snippets/snippets_gui.c:832
msgid "Import snippets"
msgstr "Import snippets"

#: src/plugin_snippets/snippets_gui.c:836
msgid "_Show as menu"
msgstr "_Show as menu"

#: src/plugin_snippets/snippets_gui.c:836
msgid "Show snippets menu"
msgstr "Show snippets menu"

#: src/plugin_snippets/snippets_gui.c:838
msgid "S_nippets Menu"
msgstr "S_nippets Menu"

#: src/plugin_snippets/snippets_leaf_insert.c:64
msgid "[cursor position or selection]"
msgstr "[cursor position or selection]"

#: src/plugin_snippets/snippets_leaf_snr.c:153
msgid "Search for: '"
msgstr "Search for: '"

#: src/plugin_snippets/snippets_leaf_snr.c:153
msgid "', replace with: '"
msgstr "', replace with: '"

#: src/plugin_snippets/snippets_wizard.c:111
msgid "Entire document"
msgstr "Entire document"

#: src/plugin_snippets/snippets_wizard.c:112
msgid "Forward from cursor position"
msgstr "Forward from cursor position"

#: src/plugin_snippets/snippets_wizard.c:113
msgid "Selection"
msgstr "Selection"

#: src/plugin_snippets/snippets_wizard.c:114
msgid "All open files"
msgstr "All open files"

#: src/plugin_snippets/snippets_wizard.c:118
msgid "Normal"
msgstr "Normal"

#: src/plugin_snippets/snippets_wizard.c:119
msgid "PERL"
msgstr "PERL"

#: src/plugin_snippets/snippets_wizard.c:130
msgid ""
"Specify a search and a replace pattern. You may use %0, %1, ...%5 "
"placeholders to ask for values when you activate this item. Give these "
"placeholders an appropriate name on the right. (Please use %% if you need "
"literal % in your string!)"
msgstr ""
"Specify a search and a replace pattern. You may use %0, %1, ...%5 "
"placeholders to ask for values when you activate this item. Give these "
"placeholders an appropriate name on the right. (Please use %% if you need "
"literal % in your string!)"

#: src/plugin_snippets/snippets_wizard.c:140
msgid "_Search for: "
msgstr "_Search for: "

#: src/plugin_snippets/snippets_wizard.c:144
msgid "Replace _with: "
msgstr "Replace _with: "

#: src/plugin_snippets/snippets_wizard.c:151
msgid "Sco_pe: "
msgstr "Sco_pe: "

#: src/plugin_snippets/snippets_wizard.c:158
msgid "Match Patter_n: "
msgstr "Match Patter_n: "

#: src/plugin_snippets/snippets_wizard.c:161
msgid "Case sensitive _matching"
msgstr "Case sensitive _matching"

#: src/plugin_snippets/snippets_wizard.c:162
msgid "Only match if case (upper/lower) is identical."
msgstr "Only match if case (upper/lower) is identical."

#: src/plugin_snippets/snippets_wizard.c:165
msgid "_Use escape chars"
msgstr "_Use escape chars"

#: src/plugin_snippets/snippets_wizard.c:166
msgid "Unescape backslash escaped characters such as \\n, \\t etc."
msgstr "Unescape backslash escaped characters such as \\n, \\t etc."

#: src/plugin_snippets/snippets_wizard.c:335
msgid ""
"The <i>before</i> text will be inserted before the cursor position or the "
"current selection, the <i>after</i> text will be inserted after the cursor "
"position or the current selection. You may use %0, %1, ...%9 placeholders to "
"ask for values when you activate this item. Give these placeholders an "
"appropriate name on the right. (Please use %% if you need literal % in your "
"string!)"
msgstr ""
"The <i>before</i> text will be inserted before the cursor position or the "
"current selection, the <i>after</i> text will be inserted after the cursor "
"position or the current selection. You may use %0, %1, ...%9 placeholders to "
"ask for values when you activate this item. Give these placeholders an "
"appropriate name on the right. (Please use %% if you need literal % in your "
"string!)"

#: src/plugin_snippets/snippets_wizard.c:344
msgid "<i>Before</i> text"
msgstr "<i>Before</i> text"

#: src/plugin_snippets/snippets_wizard.c:354
msgid "<i>After</i> text"
msgstr "<i>After</i> text"

#: src/plugin_snippets/snippets_wizard.c:362
msgid "Number"
msgstr "Number"

#: src/plugin_snippets/snippets_wizard.c:364
msgid "Name"
msgstr "Name"

#: src/plugin_snippets/snippets_wizard.c:366
msgid "Is file path"
msgstr "Is file path"

#: src/plugin_snippets/snippets_wizard.c:492
msgid "Enter the name of the branch:"
msgstr "Enter the name of the branch:"

#: src/plugin_snippets/snippets_wizard.c:584
msgid "Name of the new item:"
msgstr "Name of the new item:"

#: src/plugin_snippets/snippets_wizard.c:589
msgid "Description:"
msgstr "Description:"

#: src/plugin_snippets/snippets_wizard.c:639
msgid "Select what you want to add"
msgstr "Select what you want to add"

#: src/plugin_snippets/snippets_wizard.c:641
msgid "Branch"
msgstr "Branch"

#: src/plugin_snippets/snippets_wizard.c:643
msgid "Insert string"
msgstr "Insert string"

#: src/plugin_snippets/snippets_wizard.c:645
msgid "Search and replace pattern"
msgstr "Search and replace pattern"
