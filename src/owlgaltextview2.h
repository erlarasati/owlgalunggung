/* OwlGalunggung Code Editor
 * owlgaltextview2.h
 *
 * Copyright (C) 2017-2018 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _OWLGALTEXTVIEW2_H_
#define _OWLGALTEXTVIEW2_H_

#include <gtk/gtk.h>
#include "owl_config.h"

#define IDENTSTORING
#define UPDATE_OFFSET_DELAYED

#define MARKREGION

#ifndef NEEDSCANNING
#define MARKREGION
#endif

#ifdef MARKREGION
typedef struct {
	gpointer head;
	gpointer tail;
	gpointer last;
} Tregions;
#endif

typedef enum {
	comment_type_block,
	comment_type_line
} Tcomment_type;

typedef struct {
	gchar *so;
	gchar *eo;
	Tcomment_type type;
} Tcomment;

typedef struct {
	guint8 allsymbols[128];		/* this lookup table holds all symbols for all contexts, and is used to trigger scanning if reduced_scan_triggers is enabled */
	GArray *contexts;			/* dynamic sized array of Tcontext that translates a context number into a rownumber in the DFA table */
	GArray *matches;			/* dynamic sized array of Tpattern */
	GArray *comments;			/* array of Tcomment, has max. 256 entries, we use a guint8 as index */
	GArray *blocks; 			/* array of Tpattern_block with a guint16 as index */
	GArray *conditions;	/* array of Tpattern_condition with a guint16 as index */
} Tscantable;

typedef struct {
	GSequence *foundcaches;		/* a sorted structure of Tfound for
								   each position where the stack changes so we can restart scanning
								   on any location */
#ifdef UPDATE_OFFSET_DELAYED
	gpointer offsetupdates; /* points to the last Toffsetupdate in the list (has an embedded list) */
#endif
} Tscancache;
/********************************/
/* language manager */
/********************************/
typedef struct {
	gchar *name;
	GList *mimetypes;
	GList *tags;				/* all tags used for highlighting in this language. we use this list when
								   we want to remove all tags and want to re-highlight */
	gchar *filename;			/* the .owlgallang2 file */
	Tscantable *st;				/* NULL or complete */
	gchar *smartindentchars;
	gchar *smartoutdentchars;
#ifdef HAVE_LIBENCHANT
	gboolean default_spellcheck;
	gboolean spell_decode_entities;
#endif
	gboolean no_st;				/* no scantable, for Text, don't try to load the scantable if st=NULL */
	gboolean parsing;			/* set to TRUE when a thread is parsing the scantable already */
	gboolean in_menu; /* set to TRUE to show this language in the menu */
	gint size_table;
	gint size_contexts;
	gint size_matches;
} Towlgallang;

#define OWLGALLANG(var)  ((Towlgallang *)var)

/* Color Configuation data */
typedef enum {
	BTV_COLOR_ED_BG,
	BTV_COLOR_ED_FG,
	BTV_COLOR_WHITESPACE,
	BTV_COLOR_CURRENT_LINE,
	BTV_COLOR_RIGHT_MARGIN,
	BTV_COLOR_CURSOR,
	BTV_COLOR_SELECTION,
	BTV_COLOR_CURSOR_HIGHLIGHT,
/*	BTV_COLOR_SEARCH_BG,
	BTV_COLOR_SEARCH_FG,*/
	BTV_COLOR_COUNT
} Tbtv_colors;


/*****************************************************************/
/* stuff for the widget */
/*****************************************************************/

#define OWLGALUNGGUNG_TYPE_TEXT_VIEW            (owlgalunggung_text_view_get_type ())
#define OWLGALUNGGUNG_TEXT_VIEW(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), OWLGALUNGGUNG_TYPE_TEXT_VIEW, OwlGalunggungTextView))
#define OWLGALUNGGUNG_TEXT_VIEW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass), OWLGALUNGGUNG_TYPE_TEXT_VIEW, OwlGalunggungTextViewClass))
#define OWLGALUNGGUNG_IS_TEXT_VIEW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), OWLGALUNGGUNG_TYPE_TEXT_VIEW))
#define OWLGALUNGGUNG_IS_TEXT_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), OWLGALUNGGUNG_TYPE_TEXT_VIEW))
#define OWLGALUNGGUNG_TEXT_VIEW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), OWLGALUNGGUNG_TYPE_TEXT_VIEW, OwlGalunggungTextViewClass))

typedef struct _OwlGalunggungTextView OwlGalunggungTextView;
typedef struct _OwlGalunggungTextViewClass OwlGalunggungTextViewClass;

struct _OwlGalunggungTextView {
	GtkTextView parent;
	gpointer master;			/* points usually to self, but in the case of a slave widget
								   (two widgets showing the same buffer it will point to the master widget) */
	gpointer slave;				/* usually NULL, but might point to a slave widget */
	Towlgallang *owlgallang;			/* Towlgallang */
	gpointer doc;				/* Tdocument */
	GtkTextBuffer *buffer;
#ifdef NEEDSCANNING
	GtkTextTag *needscanning;
#ifdef HAVE_LIBENCHANT
	GtkTextTag *needspellcheck;
#endif 	/*HAVE_LIBENCHANT */
#endif
#ifdef MARKREGION
	Tregions scanning;
#ifdef HAVE_LIBENCHANT
	Tregions spellcheck;
#endif /*HAVE_LIBENCHANT*/
#endif /*MARKREGION*/
	GtkTextTag *blockmatch;
	GtkTextTag *cursortag;
	Tscancache scancache;
	guint scanner_immediate; /* event ID for the high priority scanning run */
	guint scanner_idle;			/* event ID for the idle function that handles the scanning. 0 if no idle function is running */
	guint scanner_delayed;		/* event ID for the timeout function that handles the delayed scanning. 0 if no timeout function is running */
	GTimer *user_idle_timer;
	guint user_idle;			/* event ID for the timed function that handles user idle events such as autocompletion popups */
	guint mark_set_idle;		/* event ID for the mark_set idle function that avoids showing matching block bounds while
								   you hold the arrow key to scroll quickly */
	gulong insert_text_id;
	gulong insert_text_after_id;
	gulong mark_set_id;
	gulong delete_range_id;
	gulong delete_range_after_id;

	gpointer autocomp;			/* a Tacwin* with the current autocompletion window */
	gboolean needs_autocomp;	/* a state of the widget, autocomplete is needed on user keyboard actions */
	gboolean needs_blockmatch;	/* a state of the widget, if the cursor position was changed */
	gboolean key_press_inserted_char;	/* FALSE if the key press was used by autocomplete popup, or simply not in our widget */
	gdouble button_press_line; /* line location of the button press, used in the release */
	/*gboolean key_press_was_autocomplete;  a state of the widget, if the last keypress was handled by the autocomplete popup window */
	gboolean showing_blockmatch;	/* a state of the widget if we are currently showing a blockmatch */
	gboolean insert_was_auto_indent;	/* a state of the widget if the last keypress (enter) caused
										   autoindent (so we should unindent on a closing bracket */
	guint needremovetags;	/* after we have removed all old highlighting, we set this to G_MAXUINT32, or to the
									offset up to the point where we removed the old highlighting. but after a change that
									needs highlighting we set this to the offset of the change. */
	/* next three are used for margin painting */
	gint margin_pixels_per_char;
	gint margin_pixels_chars;
	gint margin_pixels_block;
	gint margin_pixels_symbol;

	/* following options are simple true/false settings */
	gboolean enable_scanner;	/* only run scanner when TRUE, this is FALSE if the document is in the background for example */
	gboolean auto_indent;
	gboolean auto_complete;
	gboolean show_line_numbers;
	gboolean show_blocks;
	gboolean showsymbols;
	gboolean visible_spacing;
	gboolean show_right_margin;
	gboolean show_mbhl;			/* show matching block highlighting */
#ifdef HAVE_LIBENCHANT
	gboolean spell_check;
#endif
};

struct _OwlGalunggungTextViewClass {
	GtkTextViewClass parent_class;
};

GType owlgalunggung_text_view_get_type(void);
const gchar *owlgalunggung_text_view_get_lang_name(OwlGalunggungTextView *btv);
gboolean owlgalunggung_text_view_get_active_block_boundaries(OwlGalunggungTextView *btv, guint location, gboolean innerblock, GtkTextIter *so, GtkTextIter *eo);
gpointer owlgaltextview2_get_block_at_boundary_location(OwlGalunggungTextView *btv, guint offset, GtkTextIter *it1, GtkTextIter *it2, GtkTextIter *it3, GtkTextIter *it4);
gboolean owlgalunggung_text_view_get_auto_complete(OwlGalunggungTextView * btv);
void owlgalunggung_text_view_set_auto_complete(OwlGalunggungTextView * btv, gboolean enable);
gboolean owlgalunggung_text_view_get_auto_indent(OwlGalunggungTextView * btv);
void owlgalunggung_text_view_set_auto_indent(OwlGalunggungTextView * btv, gboolean enable);
void owlgaltextview2_init_globals(void);
void owlgalunggung_text_view_set_colors(OwlGalunggungTextView * btv, gchar * const *colors);
void owlgalunggung_text_view_select_language(OwlGalunggungTextView * btv, const gchar * mime, const gchar * filename);
gboolean owlgalunggung_text_view_get_show_blocks(OwlGalunggungTextView * btv);
void owlgalunggung_text_view_set_show_blocks(OwlGalunggungTextView * btv, gboolean show);
void owlgalunggung_text_view_set_show_symbols_redraw(OwlGalunggungTextView * btv, gboolean show);
gboolean owlgalunggung_text_view_get_show_line_numbers(OwlGalunggungTextView * btv);
void owlgalunggung_text_view_set_show_line_numbers(OwlGalunggungTextView * btv, gboolean show);
gboolean owlgalunggung_text_view_get_show_visible_spacing(OwlGalunggungTextView * btv);
void owlgalunggung_text_view_set_show_visible_spacing(OwlGalunggungTextView * btv, gboolean show);
gboolean owlgalunggung_text_view_get_show_right_margin(OwlGalunggungTextView * btv);
void owlgalunggung_text_view_set_show_right_margin(OwlGalunggungTextView * btv, gboolean show);
void owlgalunggung_text_view_set_font(OwlGalunggungTextView *btv, PangoFontDescription *font_desc);
gboolean owlgalunggung_text_view_get_show_mbhl(OwlGalunggungTextView * btv);
void owlgalunggung_text_view_set_show_mbhl(OwlGalunggungTextView * btv, gboolean show);
#ifdef HAVE_LIBENCHANT
void owlgalunggung_text_view_set_spell_check(OwlGalunggungTextView * btv, gboolean spell_check);
#endif
/* this functions is used in the widget in _autocomp.c */
gchar *get_line_indenting(GtkTextBuffer * buffer, GtkTextIter * iter, gboolean prevline);
void owlgalunggung_text_view_scan_cleanup(OwlGalunggungTextView * btv);
void owlgalunggung_text_view_rescan(OwlGalunggungTextView * btv);
void owlgaltextview2_schedule_scanning(OwlGalunggungTextView * btv);
gboolean owlgalunggung_text_view_in_comment(OwlGalunggungTextView * btv, GtkTextIter * its, GtkTextIter * ite);
Tcomment *owlgalunggung_text_view_get_comment(OwlGalunggungTextView * btv, GtkTextIter * it,
										 Tcomment_type preferred_type);
void owlgalunggung_text_view_multiset(OwlGalunggungTextView * btv, gpointer doc, gint view_line_numbers,
							gint view_blocks, gint autoindent, gint autocomplete, gint show_mbhl, gint enable_scanner);

GtkWidget *owlgaltextview2_new(void);
GtkWidget *owlgaltextview2_new_with_buffer(GtkTextBuffer * buffer);
GtkWidget *owlgaltextview2_new_slave(OwlGalunggungTextView * master);

#endif
