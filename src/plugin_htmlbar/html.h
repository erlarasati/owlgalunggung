/* OwlGalunggung Code Editor
 * html.h - menu/toolbar callback prototypes
 *
 * Copyright (C) 2017-2018 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __HTML_H_
#define __HTML_H_

#include "html_diag.h"			/* Ttagpopup */

typedef enum {
	linkdialog_mode_default,
	linkdialog_mode_css
} Tlinkdialog_mode;

typedef enum {
	OWLGAL_NBSP=1,
	OWLGAL_BROKEN_BAR,
	OWLGAL_SOFT_HYPHEN
} TBFSpecialChars;

void htmlbar_insert_special_char(Towlgalwin * owlgalwin, TBFSpecialChars spchar);

void general_html_menu_cb(Towlgalwin * owlgalwin, guint callback_action, GtkWidget * widget);

void insert_time_dialog(Towlgalwin * owlgalwin);
void quickanchor_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void block_tag_edit_cb(gint type, GtkWidget * widget, Towlgalwin * owlgalwin);
void p_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void div_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void columns_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void span_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void h1_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void h2_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void h3_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void h4_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void h5_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void h6_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void quickrule_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void body_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void meta_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void font_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void basefont_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void email_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void quicklist_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void frameset_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void frame_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void audio_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void embed_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void script_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void linkdialog_dialog(Towlgalwin * owlgalwin, Ttagpopup * data, Tlinkdialog_mode mode);
void audio_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void video_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void canvas_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void html5time_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);


#endif							/* __HTML_H_ */
