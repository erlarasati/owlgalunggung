/* OwlGalunggung Code Editor
 * html_form.h - menu/toolbar callback prototypes
 *
 * Copyright (C) 2017-2018 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __HTML_FORM_H_
#define __HTML_FORM_H_

#include "../owlgalunggung.h"
#include "html_diag.h"

void formdialog_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void textareadialog_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void selectdialog_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void optiondialog_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void optgroupdialog_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);
void inputdialog_dialog(Towlgalwin * owlgalwin, Ttagpopup * data, const gchar * type);
void inputdialog_rpopup(Towlgalwin * owlgalwin, Ttagpopup * data);
void buttondialog_dialog(Towlgalwin * owlgalwin, Ttagpopup * data);

#endif							/* __HTML_FORM_H_ */
