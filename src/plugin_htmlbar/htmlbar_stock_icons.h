/*
 * OwlGalunggung Code Editor
 * htmlbar_stock_icons.h
 *
 * Copyright (C) 2017-2018 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HTMLBAR_STOCK_ICONS_H_
#define HTMLBAR_STOCK_ICONS_H_


#define OWLGAL_STOCK_ADDR				"owlgal-stock-addr"
#define OWLGAL_STOCK_ACRONYM			"owlgal-stock-acronym"
#define OWLGAL_STOCK_ANCHOR				"owlgal-stock-anchor"
#define OWLGAL_STOCK_BODY				"owlgal-stock-body"
#define OWLGAL_STOCK_BOLD				"owlgal-stock-bold"
#define OWLGAL_STOCK_BRBREAK			"owlgal-stock-brbreak"
#define OWLGAL_STOCK_BREAKALL			"owlgal-stock-breakall"
#define OWLGAL_STOCK_CENTER				"owlgal-stock-center"
#define OWLGAL_STOCK_CITE				"owlgal-stock-cite"
#define OWLGAL_STOCK_CODE				"owlgal-stock-code"
#define OWLGAL_STOCK_COMMENT			"owlgal-stock-comment"
#define OWLGAL_STOCK_CONTEXT			"owlgal-stock-context"
#define OWLGAL_STOCK_CSS_SMALL			"owlgal-stock-css-small"
#define OWLGAL_STOCK_CSSDIV				"owlgal-stock-css-div"
#define OWLGAL_STOCK_CSSNEWSTYLE		"owlgal-stock-css-newstyle"
#define OWLGAL_STOCK_CSSSPAN			"owlgal-stock-css-span"
#define OWLGAL_STOCK_CSSSTYLE			"owlgal-stock-css-style"
#define OWLGAL_STOCK_CSS_COLUMNS		"owlgal-stock-css-columns"
#define OWLGAL_STOCK_DEL				"owlgal-stock-del"
#define OWLGAL_STOCK_DFN				"owlgal-stock-dfn"
#define OWLGAL_STOCK_EDIT_TAG			"owlgal-stock-edit-tag"
#define OWLGAL_STOCK_EMAIL				"owlgal-stock-email"
#define OWLGAL_STOCK_FONT_BASE			"owlgal-stock-font-base"
#define OWLGAL_STOCK_FONT				"owlgal-stock-font"
#define OWLGAL_STOCK_FONTH1				"owlgal-stock-fonth1"
#define OWLGAL_STOCK_FONTH2				"owlgal-stock-fonth2"
#define OWLGAL_STOCK_FONTH3				"owlgal-stock-fonth3"
#define OWLGAL_STOCK_FONTH4				"owlgal-stock-fonth4"
#define OWLGAL_STOCK_FONTH5				"owlgal-stock-fonth5"
#define OWLGAL_STOCK_FONTH6				"owlgal-stock-fonth6"
#define OWLGAL_STOCK_FONTM1				"owlgal-stock-fontm1"
#define OWLGAL_STOCK_FONTP1				"owlgal-stock-fontp1"
#define OWLGAL_STOCK_FONTPRE			"owlgal-stock-fontpre"
#define OWLGAL_STOCK_FONTSUB			"owlgal-stock-fontsub"
#define OWLGAL_STOCK_FONTSUPER			"owlgal-stock-fontsuper"
#define OWLGAL_STOCK_FORM_CHECK			"owlgal-stock-form-check"
#define OWLGAL_STOCK_FORM_HIDDEN		"owlgal-stock-form-hidden"
#define OWLGAL_STOCK_FORM_OPTION		"owlgal-stock-form-option"
#define OWLGAL_STOCK_FORM_OPTIONGROUP	"owlgal-stock-form-optiongroup"
#define OWLGAL_STOCK_FORM_RADIO			"owlgal-stock-form-radio"
#define OWLGAL_STOCK_FORM_SELECT		"owlgal-stock-form-select"
#define OWLGAL_STOCK_FORM_SUBMIT		"owlgal-stock-form-submit"
#define OWLGAL_STOCK_FORM_TEXT			"owlgal-stock-form-text"
#define OWLGAL_STOCK_FORM_TEXTAREA		"owlgal-stock-form-textarea"
#define OWLGAL_STOCK_FORM				"owlgal-stock-form"
#define OWLGAL_STOCK_FRAME_BASE			"owlgal-stock-frame-base"
#define OWLGAL_STOCK_FRAME_NO			"owlgal-stock-frame-no"
#define OWLGAL_STOCK_FRAME				"owlgal-stock-frame"
#define OWLGAL_STOCK_FRAME2				"owlgal-stock-frame2"
#define OWLGAL_STOCK_FRAMESET			"owlgal-stock-frameset"
#define OWLGAL_STOCK_FRAMESET2			"owlgal-stock-frameset2"
#define OWLGAL_STOCK_FRAME_WIZARD		"owlgal-stock-frame-wizard"
#define OWLGAL_STOCK_HEADINGS			"owlgal-stock-headings"
#define OWLGAL_STOCK_HRULE				"owlgal-stock-hrule"
#define OWLGAL_STOCK_IMAGE				"owlgal-stock-image"
#define OWLGAL_STOCK_INS				"owlgal-stock-ins"
#define OWLGAL_STOCK_ITALIC				"owlgal-stock-italic"
#define OWLGAL_STOCK_KBD				"owlgal-stock-kbd"
#define OWLGAL_STOCK_LINK_STYLESHEET	"owlgal-stock-link-stylesheet"
#define OWLGAL_STOCK_LIST_DD			"owlgal-stock-list-dd"
#define OWLGAL_STOCK_LIST_DL			"owlgal-stock-list-dl"
#define OWLGAL_STOCK_LIST_DT			"owlgal-stock-list-dt"
#define OWLGAL_STOCK_LIST_LI			"owlgal-stock-list-li"
#define OWLGAL_STOCK_LIST_OL			"owlgal-stock-list-ol"
#define OWLGAL_STOCK_LIST_UL			"owlgal-stock-list-ul"
#define OWLGAL_STOCK_LIST				"owlgal-stock-list"
#define OWLGAL_STOCK_MULTITHUMBS		"owlgal-stock-multithumbs"
#define OWLGAL_STOCK_NBSP				"owlgal-stock-nbsp"
#define OWLGAL_STOCK_PARAGRAPH			"owlgal-stock-paragraph"
#define OWLGAL_STOCK_HYSS_XX			"owlgal-stock-hyss-xx"
#define OWLGAL_STOCK_QUICKSTART			"owlgal-stock-quickstart"
#define OWLGAL_STOCK_RIGHT				"owlgal-stock-right"
#define OWLGAL_STOCK_SAMP				"owlgal-stock-samp"
#define OWLGAL_STOCK_SCRIPT				"owlgal-stock-script"
#define OWLGAL_STOCK_STRIKEOUT			"owlgal-stock-strikeout"
#define OWLGAL_STOCK_TABLE_CAPTION		"owlgal-stock-table-caption"
#define OWLGAL_STOCK_TABLE_TD			"owlgal-stock-table-td"
#define OWLGAL_STOCK_TABLE_TD2			"owlgal-stock-table-td2"
#define OWLGAL_STOCK_TABLE_TH			"owlgal-stock-table-th"
#define OWLGAL_STOCK_TABLE_TH2			"owlgal-stock-table-th2"
#define OWLGAL_STOCK_TABLE_TR			"owlgal-stock-table-tr"
#define OWLGAL_STOCK_TABLE_TR2			"owlgal-stock-table-tr2"
#define OWLGAL_STOCK_TABLE				"owlgal-stock-table"
#define OWLGAL_STOCK_TABLE2				"owlgal-stock-table2"
#define OWLGAL_STOCK_TABLE_WIZARD		"owlgal-stock-table-wizard"
#define OWLGAL_STOCK_THUMBNAIL			"owlgal-stock-thumbnail"
#define OWLGAL_STOCK_UNDERLINE			"owlgal-stock-underline"
#define OWLGAL_STOCK_VAR				"owlgal-stock-var"
#define OWLGAL_STOCK_ARTICLE			"owlgal-stock-article"
#define OWLGAL_STOCK_ASIDE				"owlgal-stock-aside"
#define OWLGAL_STOCK_FIGCAPTION			"owlgal-stock-figcaption"
#define OWLGAL_STOCK_FIGURE				"owlgal-stock-figure"
#define OWLGAL_STOCK_FOOTER				"owlgal-stock-footer"
#define OWLGAL_STOCK_HEADER				"owlgal-stock-header"
#define OWLGAL_STOCK_HGROUP				"owlgal-stock-hgroup"
#define OWLGAL_STOCK_MARK				"owlgal-stock-mark"
#define OWLGAL_STOCK_NAV				"owlgal-stock-nav"
#define OWLGAL_STOCK_SECTION			"owlgal-stock-section"
#define OWLGAL_STOCK_AUDIO				"owlgal-stock-audio"
#define OWLGAL_STOCK_VIDEO				"owlgal-stock-video"
#define OWLGAL_STOCK_CANVAS				"owlgal-stock-canvas"
#define OWLGAL_STOCK_HTML5TIME			"owlgal-stock-html5time"

#endif /* HTMLBAR_STOCK_ICONS_H_ */
