/* OwlGalunggung Code Editor
 * image.h - the thumbnail/multi-thumbnail dialogs
 *
 * Copyright (C) 2017-2018 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __IMAGE_H_
#define __IMAGE_H_

#include "../owlgalunggung.h"

void thumbnail_insert_dialog(Towlgalwin * owlgalwin);
void multi_thumbnail_dialog(Towlgalwin * owlgalwin);

#endif							/* __IMAGE_H_ */
